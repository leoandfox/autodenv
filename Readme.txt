PROJET D'EXCELLENCE

*Pré-requis:
 - Web Serveur: Nodejs , Sails Js
 - Deployment Tool : ansible
 - Versionning : Git

*Ouverture du port du serveur web: 
firewall-cmd --zone=public --add-port=1337/tcp --permanent
firewall-cmd --reload