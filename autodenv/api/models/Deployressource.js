/**
 * Deployressource.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:  {
      required: true,
      type: 'string',
      example: 'Serveur Web',
      description: 'The server\'s name of app.',
    },
    playbook_to_use:  {
      required: true,
      type: 'string',
      example: 'Sails js, Django, Spring',
      description: 'Playbook use for deployment',
    },
    port_of_your_app:  {
      required: true,
      type: 'string',
      example: '80',
      description: 'The server\'s name of app.',
    },
    ip_server_cible:  {
      required: true,
      type: 'string',
      example: '192.168.48.135',
      description: 'Ip address of your server',
    },
    port_ssh:  {
      required: true,
      type: 'string',
      example: '22',
      description: 'Port ssh of your server',
    },
    user:  {
      required: true,
      type: 'string',
      example: 'admin_web',
      description: 'User on your server',
    },
    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password of your server'
    },
    git_repository:  {
      required: true,
      type: 'string',
      example: 'https://gitlab.com/leoandfox/autodenv.git',
      description: 'The link to the code repository of your code',
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

