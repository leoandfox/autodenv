const replace = require('replace-in-file');
var fs = require('fs');
const {
  exec
} = require("child_process");
module.exports = {

  friendlyName: 'Ansible deploy',


  description: 'Deploy Service On a server',


  extendedDescription: `Documentation is not present`,


  inputs: {
    name: {
      required: true,
      type: 'string',
      example: 'Serveur Web',
      description: 'The server\'s name of app.',
    },
    playbook_to_use: {
      required: true,
      type: 'string',
      example: 'Sails js, Django, Spring',
      description: 'Playbook use for deployment',
    },
    port_of_your_app: {
      required: true,
      type: 'string',
      example: '80',
      description: 'The server\'s name of app.',
    },
    ip_server_cible: {
      required: true,
      type: 'string',
      example: '192.168.48.135',
      description: 'Ip address of your server',
    },
    port_ssh: {
      required: true,
      type: 'string',
      example: '22',
      description: 'Port ssh of your server',
    },
    user: {
      required: true,
      type: 'string',
      example: 'admin_web',
      description: 'User on your server',
    },
    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password of your server'
    },
    git_repository: {
      required: true,
      type: 'string',
      example: 'https://gitlab.com/leoandfox/autodenv.git',
      description: 'The link to the code repository of your code',
    },
  },


  exits: {

    success: {
      description: 'New deployment was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'Something is invalid in your request. Please retry !',
      extendedDescription: 'If this request was sent from a graphical user interface, the request ' +
        'parameters should have been validated/coerced _before_ they were sent.'
    },

  },
  fn: async function (inputs) {

    console.log("Controller deploy ressource");
    var name = inputs.name;
    var playbook_to_use = inputs.playbook_to_use;
    var port_of_your_app = inputs.port_of_your_app;
    var ip_server_cible = inputs.ip_server_cible;
    var port_ssh = inputs.port_ssh;
    var user = inputs.user;
    var password = inputs.password;
    var git_repository = inputs.git_repository;

    console.log("ip server " + ip_server_cible);
    fs.readFile('hosts', 'utf8', function (err, data) {
      if (err) {
        return console.log(err);
      } else {
        console.log("data : " + data);
      }
      var result = data.replace(/AUTODENV_IP_SERVER/g, ip_server_cible);
      result = result.replace(/AUTODENV_USER/g, user);
      result = result.replace(/AUTODENV_PASSWORD/g, password);
      result = result.replace(/AUTODENV_PORT/g, port_ssh);
      console.log("Résultat du remplacement remplaced : " + result);

      fs.writeFile(ip_server_cible, result, 'utf8', function (err) {
        if (err) return console.log(err);
        var commande = "export ANSIBLE_HOST_KEY_CHECKING=False && ansible-playbook -i " + ip_server_cible + " install-docker.yml"
        exec(commande, (error, stdout, stderr) => {
          if (error) {
            console.log(`error: ${error.message}`);
            return;
          }
          if (stderr) {
            console.log(`stderr: ${stderr}`);
            // return;
          }
          console.log(`stdout: ${stdout}`);
          console.log("Start installing wordpress");
          fs.copyFile(ip_server_cible, "docker-ansible-wordpress/hosts", (err) => {
            if (err) throw err;
            console.log('hosts was copied to docker-ansible-wordpress/hosts' + ip_server_cible);
            var commande2 = "export ANSIBLE_HOST_KEY_CHECKING=False && ansible-playbook -i " + ip_server_cible + " deploy-student-list.yml"
            exec(commande2, (error, stdout, stderr) => {
              if (error) {
                console.log(`error install wordpress: ${error.message}`);
                //return;
              }
              if (stderr) {
                console.log(`stderr install wordpress: ${stderr}`);
                // return;
              }
              console.log(`stdout install wordpress = : ${stdout}`);
            });
          });

        });

      });
    });
    console.log("end of processing !");
    return {

    };


  }
};
